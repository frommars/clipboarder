﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Clipboarder
{
    public class Animator
    {
        private readonly MainWindow _window;

        public Animator(MainWindow window)
        {
            this._window = window;
        }

        public void ShowFromBottom()
        {
            var to = SystemParameters.PrimaryScreenHeight - _window.Height;
            var myAnim = new DoubleAnimation(_window.Top, to, TimeSpan.FromSeconds(0.15))
            {
                EasingFunction = new CubicEase()
            };
            //myAnim.Completed += onComplete;
            _window.BeginAnimation(Window.TopProperty, myAnim);
            
//            myAnim.EasingFunction.Ease(CubicEase.EasingModeProperty.AddOwner(myAnim));
        }

        public void HideToBottom(Action onComplete)
        {
            var to = SystemParameters.PrimaryScreenHeight;
            var myAnim = new DoubleAnimation(_window.Top, to, TimeSpan.FromSeconds(0.15))
            {
                EasingFunction = new CubicEase()
            };
            myAnim.Completed += CallOnComplete;
            _window.BeginAnimation(Window.TopProperty, myAnim);

            void CallOnComplete(object sender, EventArgs e)
            { 
                onComplete?.Invoke();
                onComplete = null;
            }
        }
    }
}
