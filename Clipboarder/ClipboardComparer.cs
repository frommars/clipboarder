﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Clipboarder
{
    public static class ClipboardComparer
    {
        public static bool CompateText(string current)
        {
            var newObject = Clipboard.GetText();
            return current.GetHashCode() == newObject.GetHashCode();
        }

        public static bool CompareImage(BitmapSource current)
        {
            var newObject = Clipboard.GetImage();
            return current.GetHashCode() == newObject.GetHashCode();
        }

        public static bool CompareAudio(Stream current)
        {
            var newObject = Clipboard.GetAudioStream();
            return current.GetHashCode() == newObject.GetHashCode();
        }

        public static bool CompareData(StringCollection current)
        {
            var newObject = Clipboard.GetFileDropList();
            return current.GetHashCode() == newObject.GetHashCode();
        }
    }
}
