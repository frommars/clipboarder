﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Interop;

namespace Clipboarder
{
    public sealed class ClipboardMonitor : IDisposable
    {
        private readonly HwndSource _hwndSource = new HwndSource(0, 0, 0, 0, 0, 0, 0, null, NativeWinApi.HWND_MESSAGE);
        private int _lastOwnerId;
        private DateTime _lastCopied = DateTime.MinValue;
        private IDataObject _currentData;

        public ClipboardMonitor()
        {
            _hwndSource.AddHook(WndProc);
            NativeWinApi.AddClipboardFormatListener(_hwndSource.Handle);
        }

        public void Dispose()
        {
            NativeWinApi.RemoveClipboardFormatListener(_hwndSource.Handle);
            _hwndSource.RemoveHook(WndProc);
            _hwndSource.Dispose();
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == NativeWinApi.WM_CLIPBOARDUPDATE)
            {
                if (CheckTimer())
                {
                    return IntPtr.Zero;
                }

                var currentOwnerId = Process.GetProcessById((int) NativeWinApi.GetClipboardOwnerProcessID()).Id;
                if (_lastOwnerId == currentOwnerId)
                {
                    if (CheckObject())
                    {
                        return IntPtr.Zero;
                    }
                }
                else
                {
                    _lastOwnerId = currentOwnerId;
                }

                OnClipboardContentChanged?.Invoke(this, EventArgs.Empty);
                handled = true;
            }

            return IntPtr.Zero;
        }

        private bool CheckObject()
        {
            if (_currentData != null)
            {                
//                if(Clipboard.ContainsText())
            }
            else
            {
                _currentData = Clipboard.GetDataObject();
            }

            return false;
        }
        
        private bool CheckTimer()
        {
            var c = false;
            Console.WriteLine("Current request: " + (DateTime.Now - _lastCopied).Milliseconds);
            if ((DateTime.Now - _lastCopied).Milliseconds < 100)
            {
                c = true;
            }

            _lastCopied = DateTime.Now;
            return c;
        }

        /// <summary>
        /// Occurs when the clipboard content changes.
        /// </summary>
        public event EventHandler OnClipboardContentChanged;
    }
}