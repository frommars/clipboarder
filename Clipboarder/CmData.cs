﻿using System;

namespace Clipboarder
{
    public class CmData
    {
        public object Data { get; set; }
        public decimal Size { get; set; }
        public int Id { get; set; }
        public DateTime RecordTime { get; set; }
        public string FileExtension;
        public string FileType;
        public string FileArea;
        public string Tag;
    }
}
