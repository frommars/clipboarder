﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clipboarder
{
    public class ConnectorSQLite: IDbConnector
    {
//        private SQLiteConnection _connection;

        public void ConnectToDb()
        {
//            _connection = new SQLiteConnection("cbm.db", true);
            
            SQLiteConnection.CreateFile("cbm.db3");
            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
            {
                connection.ConnectionString = "Data Source = " + "cbm.db3";
                connection.Open();

                using (SQLiteCommand command = new SQLiteCommand(connection))
                {
                    command.CommandText = @"CREATE TABLE [cbmEntry] (
                    [cbmId] integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                    [cbmName] char(100) NOT NULL,
                    [cbmType] char(100) NOT NULL,
                    [cbmSize] int NOT NULL,
                    [cbmAppName] char(100) NOT NULL,
                    [cbmFileFormat] char(100) NOT NULL,
                    [cbmCreateData] char(100) NOT NULL,
                    [cbmCopyCount] char(100) NOT NULL,
                    [cbmLabel] char(100) NOT NULL
                    );";
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void AddRecord(object data)
        {
           
        }

        public object GetRecord(object filter)
        {
            throw new NotImplementedException();
        }

        public object GetAllRecords()
        {
            throw new NotImplementedException();
        }
    }
}
