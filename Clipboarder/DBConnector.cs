﻿using System;
using System.Diagnostics;
using System.Windows;
using Clipboarder.DBclasses;

namespace Clipboarder
{
    public class DBConnector
    {
        private readonly IDbConnector _dbConnector;
        public DBConnector(DBS currentDbs)
        {
            switch (currentDbs)
            {
                    case DBS.SqLite: _dbConnector = new ConnectorLiteDb();
                        break;
                    case DBS.Mongo: _dbConnector = new ConnectorLiteDb();
                        break;
                    case DBS.Local: _dbConnector = new TempDictionaryConnector();
                        break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(currentDbs), currentDbs, null);
            }

            _dbConnector.ConnectToDb();
        }

        public void CreateDataBase()
        {

        }

        public void AddRecord(object data)
        {
            _dbConnector.AddRecord(data);
        }

        public object GetRecordsList()
        {
            return _dbConnector.GetAllRecords();
        }

        public void OnClipboardChange(object sender, EventArgs e)
        {
            var text = Clipboard.GetText();

            var clipBoardOwnerProcess = Process.GetProcessById((int)NativeWinApi.GetClipboardOwnerProcessID());


            var processName = clipBoardOwnerProcess.ProcessName;
            var processWindowTitle = clipBoardOwnerProcess.MainWindowTitle;
            var mainModuleFileName = clipBoardOwnerProcess.MainModule.FileName;

            var newDataObject = new ContentObject
            {
                Data = text
            };

            var newRecord = new ClipboardObject
            {
                ContentType = 0,
                ContentKind = 0,
                DateCreated = DateTime.Now.ToFileTimeUtc(),
                LifeTime = long.MaxValue,
                Size = text.Length,
                Label = null,
                TimesCopied = 0,
                ContentId = newDataObject.ContentId
            };
        }
    }   

    public enum DBS
    {
        SqLite,
        Mongo,
        Local
    }
}
