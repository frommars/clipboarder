﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteDB;

namespace Clipboarder.DBclasses
{
    public class ClipboardObject
    {
        [BsonId]
        public int Id { get; set; }
        public long DateCreated { get; set; }
        public long LifeTime { get; set; }
        public int ContentType { get; set; }
        public int ContentKind { get; set; }

        public int Size { get; set; }
        public string Label { get; set; }
        public uint TimesCopied { get; set; }
        public int ContentId { get; set; }
    }

    public class ContentObject
    {
        [BsonId]
        public int ContentId { get; set; }
        public object Data { get; set; }
    }
}
