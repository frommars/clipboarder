﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;

namespace Clipboarder
{
    public class HotKeys
    {
            [DllImport("user32.dll")]
            private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

            [DllImport("user32.dll")]
            private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

            private const int HOTKEY_ID = 9000;

            //Modifiers:
            private const uint MOD_NONE = 0x0000; //(none)
            private const uint MOD_ALT = 0x0001; //ALT
            private const uint MOD_CONTROL = 0x0002; //CTRL
            private const uint MOD_SHIFT = 0x0004; //SHIFT
            private const uint MOD_WIN = 0x0008; //WINDOWS
                                                 //CAPS LOCK:
            private const uint VK_CAPITAL = 0x14;

            private IntPtr _windowHandle;
            private HwndSource _source;

            private Action _callback;

            public HotKeys(MainWindow mw, Action callback)
            {
                Console.WriteLine(mw);
                _windowHandle = new WindowInteropHelper(mw).Handle;
                _source = HwndSource.FromHwnd(_windowHandle);
                _source.AddHook(HwndHook);
                _callback = callback;
                RegisterHotKey(_windowHandle, HOTKEY_ID, MOD_CONTROL, VK_CAPITAL); //CTRL + CAPS_LOCK
            }

            private IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
            {
                const int WM_HOTKEY = 0x0312;
                switch (msg)
                {
                    case WM_HOTKEY:
                        switch (wParam.ToInt32())
                        {
                            case HOTKEY_ID:
                                int vkey = (((int)lParam >> 16) & 0xFFFF);
                                if (vkey == VK_CAPITAL)
                                {
                                // tblock.Text += "CapsLock was pressed" + Environment.NewLine;
                                    _callback?.Invoke();
                                }
                                handled = true;
                                break;
                        }
                        break;
                }
                return IntPtr.Zero;
            }

            public void OnClosed(EventArgs e)
            {
                _source.RemoveHook(HwndHook);
                UnregisterHotKey(_windowHandle, HOTKEY_ID);
            }
        }
}
