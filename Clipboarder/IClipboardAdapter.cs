﻿using System;
using System.Windows;

namespace Clipboarder
{
    public interface IClipboardAdapter
    {
        event Action<object> ClipboardChanged;
        void InitClipboardTool(Window mainWindow);        
        void DisposeClipboardTool();
    }
}
