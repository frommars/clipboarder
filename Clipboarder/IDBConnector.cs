﻿namespace Clipboarder
{
    public interface IDbConnector
    {
        void ConnectToDb();
        void AddRecord(object data);
        object GetRecord(object filter);
        object GetAllRecords();
    }
}
