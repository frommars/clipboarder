﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Clipboarder.UIBuilders;
using Color = System.Windows.Media.Color;
using Rectangle = System.Windows.Shapes.Rectangle;

namespace Clipboarder
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ClipboardMonitor monitor = new ClipboardMonitor();
        private HotKeys _hk;
        private Animator _myAnimator;
        private BlurEffect _blurEffect;
        private DBConnector _dbcon;
        private Animator anim;
        private bool _isShown = true;

        #region builtin

        public MainWindow()
        {
            InitializeComponent();
            _dbcon = new DBConnector(DBS.Local);
            monitor.OnClipboardContentChanged += OnClipboardChanged;
            ResizeDynamic();
            MainRectangle.Width = mainGrid.Width;
            var rand = new Random();            
            foreach (var stackPanel1Child in StackPanel1.Children)
            {
                if (!(stackPanel1Child is Rectangle child)) continue;
                child.Margin = new Thickness(2.5f);
                child.MaxWidth = SystemParameters.PrimaryScreenWidth * 0.09;
                child.Width = child.MaxWidth;
                var r = Convert.ToByte(rand.Next(0, 255));
                var g = Convert.ToByte(rand.Next(0, 255));
                var b = Convert.ToByte(rand.Next(0, 255));
                child.Fill = new SolidColorBrush(Color.FromArgb(0xFF, r, g, b));
            }
            
        }



        protected override void OnSourceInitialized(EventArgs e)
        {
            _hk = new HotKeys(this, HotkeyPressed);
            base.OnSourceInitialized(e);
            anim = new Animator(this);
            _isShown = true;
        }

        public void HotkeyPressed()
        {
            if (_isShown)
            {
                HideMainWindow();
            }
            else
            {
                ShowMainWindow();
            }
        }

        private void ShowMainWindow()
        {
            if (IsVisible)
            {
                if (WindowState == WindowState.Minimized)
                {
                    WindowState = WindowState.Normal;
                    anim.ShowFromBottom();
                }
                //                MainWindow.Activate();
            }
            else
            {
                this.Show();
                anim.ShowFromBottom();
            }

            _isShown = true;
        }

        private void HideMainWindow()
        {
            //            if (MainWindow != null && MainWindow.IsVisible)
            //            {
            //                if (MainWindow.WindowState == WindowState.Normal)
            //                {
            //                    MainWindow.WindowState = WindowState.Minimized;
            anim.HideToBottom(Hide);
            //                }
            //            }

            _isShown = false;
        }

        protected override void OnClosed(EventArgs e)
        {
            _hk.OnClosed(e);
            base.OnClosed(e);
        }
        #endregion

        private void ResizeDynamic()
        {
            Left = 0;
            Width = SystemParameters.PrimaryScreenWidth;
            Height = SystemParameters.PrimaryScreenHeight * 0.2f;
            Top = SystemParameters.PrimaryScreenHeight - Height;
        }

        private void OnClipboardChanged(object sender, EventArgs e)
        {
            //            if (Clipboard.ContainsText())
            //            {
            //                mainLabel.Content = Clipboard.GetText();
            //            }

            

            var clipBoardOwnerProcess = Process.GetProcessById((int)NativeWinApi.GetClipboardOwnerProcessID());
            

            var processName = clipBoardOwnerProcess.ProcessName;
            var processWindowTitle = clipBoardOwnerProcess.MainWindowTitle;
            var mainModuleFileName = clipBoardOwnerProcess.MainModule.FileName;
            //var mainModuleFileName = GetActiveWindowTitle(clipBoardOwnerProcess);

            AddRectAlt(Clipboard.GetText(), clipBoardOwnerProcess.MainModule.FileName);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // _myAnimator.HideToBottom(OnHideComplete);
            // AddRectAlt("From button");
            ConsoleAll();
        }

        private void OnHideComplete(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                Hide();
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _myAnimator = new Animator(this);
            _blurEffect = new BlurEffect(this);
        }

        private void ListViewScrollViewer_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToHorizontalOffset(scv.HorizontalOffset - e.Delta);;
            e.Handled = true;
        }

        private void AddRect()
        {
            var canvas = new Canvas();
            var rect = new Rectangle();
            rect.MaxWidth = SystemParameters.PrimaryScreenWidth * 0.09;
            rect.Width = rect.MaxWidth;
            rect.RadiusX = rect.RadiusY = 15;
            rect.Margin = new Thickness(5);
            var rand = new Random();
            var r = Convert.ToByte(rand.Next(0, 255));
            var g = Convert.ToByte(rand.Next(0, 255));
            var b = Convert.ToByte(rand.Next(0, 255));
            rect.Fill = new SolidColorBrush(Color.FromArgb(0xaa, r, g, b));
//            rect.Fill = new SolidColorBrush(Color.FromArgb(0xFF,0x01,0x43,0xA4));
            //rect.Effect = new DropShadowEffect();
            canvas.MaxWidth = SystemParameters.PrimaryScreenWidth * 0.09;
            canvas.Background = new SolidColorBrush(Color.FromArgb(0xaa, r, g, b));            
            canvas.Width = rect.MaxWidth;
            var textBlock = new TextBlock
            {
                Text = "Content",
                FontSize = 24,
                TextAlignment = TextAlignment.Center
            };
            canvas.Children.Add(rect);
            canvas.Children.Add(textBlock);
            StackPanel1.Children.Add(canvas);
        }

        private void AddRectAlt(string text, string filePath)
        {
//            _dbcon.AddRecord(new ClipData
//            {
//                Data = text,
//                Size = text.Length
//            });
//            mainLabel.Content = text;
//            
            StackPanel1.Children.Add(DataRectBuilder.GetDataGrid());
        }

        private void AddImageRect(Bitmap image, string iconPath)
        {

        }

        private void ConsoleAll()
        {
            var listData = (List<ClipData>)_dbcon.GetRecordsList();
            foreach (var clipData in listData)
            {
                Console.WriteLine("Record num:"+ clipData.Id + " :: " +clipData.Data);
            }
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll")]
        private static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        public string GetActiveWindowTitle(Process currentProc)
        {
            var handle = GetForegroundWindow();
            var fileName = "";
            var name = "";
            GetWindowThreadProcessId(handle, out var pid);

            var p = currentProc;
            var processname = p.ProcessName;

            switch (processname)
            {
                case "explorer": //metro processes
                case "WWAHost":
                    name = GetTitle(handle);
                    return name;
                default:
                    break;
            }
            var wmiQuery = string.Format("SELECT ProcessId, ExecutablePath FROM Win32_Process WHERE ProcessId LIKE '{0}'", pid.ToString());
            var pro = new ManagementObjectSearcher(wmiQuery).Get().Cast<ManagementObject>().FirstOrDefault();
            fileName = (string)pro?["ExecutablePath"];
            // Get the file version
            var myFileVersionInfo = FileVersionInfo.GetVersionInfo(fileName);
            // Get the file description
            name = myFileVersionInfo.FileDescription;
            if (name == "")
                name = GetTitle(handle);

            return name;
        }

        public string GetTitle(IntPtr handle)
        {
            var windowText = "";
            const int nChars = 256;
            var buff = new StringBuilder(nChars);
            if (GetWindowText(handle, buff, nChars) > 0)
            {
                windowText = buff.ToString();
            }
            return windowText;
        }

    }
}
