﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Clipboarder
{
    public static class NativeWinApi
    {
        /// <summary>
        /// To find message-only windows, specify HWND_MESSAGE in the hwndParent parameter of the FindWindowEx function.
        /// </summary>
        public static IntPtr HWND_MESSAGE = new IntPtr(-3);

        /// <summary>
        /// Sent when the contents of the clipboard have changed.
        /// </summary>
        public const int WM_CLIPBOARDUPDATE = 0x031D;

        public const int WM_APP = 0x8000;

        /// <summary>
        /// Places the given window in the system-maintained clipboard format listener list.
        /// </summary>
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool AddClipboardFormatListener(IntPtr hwnd);

        /// <summary>
        /// Removes the given window from the system-maintained clipboard format listener list.
        /// </summary>
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool RemoveClipboardFormatListener(IntPtr hwnd);

        [SuppressUnmanagedCodeSecurity]
        internal static class SafeNativeMethods
        {
            [DllImport("user32.dll")]
            public static extern IntPtr GetClipboardOwner();
        }

        [SuppressUnmanagedCodeSecurity]
        internal static class UnsafeNativeMethods
        {
            //The return value is the identifier of the thread that created the window. 
            [DllImport("user32.dll", SetLastError = true)]
            public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);
        }

        public static uint GetClipboardOwnerProcessID()
        {
            uint processId = 0;
            UnsafeNativeMethods.GetWindowThreadProcessId(SafeNativeMethods.GetClipboardOwner(), out processId);
            return processId;
        }

        public static uint GetClipboardOwnerProcessID(ref uint threadId)
        {
            uint processId = 0;
            threadId = UnsafeNativeMethods.GetWindowThreadProcessId(SafeNativeMethods.GetClipboardOwner(), out processId);
            return processId;
        }
    }
}