﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Documents;

namespace ColorPalette
{
    class PaletteUtil
    {
        public static Color GetMainColor(Bitmap bitmap)
        {
            int thumbSize = 32;
            Dictionary<Color, int> colors = new Dictionary<Color, int>();

            Bitmap thumbBmp = bitmap;

            for (int i = 0; i < thumbSize; i++)
            {
                for (int j = 0; j < thumbSize; j++)
                {
                    Color col = thumbBmp.GetPixel(i, j);
                    if (colors.ContainsKey(col))
                        colors[col]++;
                    else
                        colors.Add(col, 1);
                }
            }

            List<KeyValuePair<Color, int>> keyValueList =
                new List<KeyValuePair<Color, int>>(colors);

            keyValueList.Sort((firstPair, nextPair) => nextPair.Value.CompareTo(firstPair.Value));
            string top10Colors = "";
            for (int i = 0; i < 10; i++)
            {
                top10Colors += string.Format("\n {0}. {1} > {2}",
                    i, keyValueList[i].Key.ToString(), keyValueList[i].Value);
               Console.WriteLine(top10Colors[i]);
            }
           

            return keyValueList[1].Key;
        }

        public bool ThumbnailCallback() { return false; }
    }

    public static class PictureAnalysis
    {
        public static List<Color> TenMostUsedColors { get; private set; }
        public static List<int> TenMostUsedColorIncidences { get; private set; }

        public static Color MostUsedColor { get; private set; }
        public static int MostUsedColorIncidence { get; private set; }

        private static int pixelColor;

        private static Dictionary<int, int> dctColorIncidence;

        public static Color MostColor(Bitmap bitmap)
        {
            using (bitmap)
            {
                var filteredBitmap = FilteredHashset(bitmap);
                var topColor = filteredBitmap.GroupBy(color => color).OrderByDescending(grp => grp.Count()).Select(grp => grp.Key)
                    .Take(1);
                var mostUsedColors =
                    GetPixels(bitmap)
                        .GroupBy(color => color)
                        .OrderByDescending(grp => grp.Count())
                        .Select(grp => grp.Key)
                        .Take(1);
                Color mostusedcolor = new Color();
                foreach (var color in topColor)
                {
                    Console.WriteLine("Color {0}", color);
                    mostusedcolor = color;
                }
                return mostusedcolor;
            }
        }

        private static HashSet<Color> FilteredHashset(Bitmap bitmap)
        {
            var filtered = new HashSet<Color>();
            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    var pixel = bitmap.GetPixel(x, y);
                    if (Filter(pixel))
                    {
                        filtered.Add(pixel);
                    }
                }
            }

            return filtered;
        }

        public static Bitmap FilteredBitmap(Bitmap bitmap)
        {
            var filtered = new Bitmap(bitmap.Width, bitmap.Height);
//            filtered.LockBits(
//                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
//                ImageLockMode.WriteOnly,
//                PixelFormat.Format32bppRgb);
            int totalPixels = 0;
            int totalNewPixels = 0;
            var hbit = filtered.GetHbitmap();
            var buffer = new List<Pixel>();
            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    totalPixels++;
                    var pixel = bitmap.GetPixel(x, y);
                    if (Filter(pixel))
                    {
                        filtered.SetPixel(x,y,pixel);
                        totalNewPixels++;
//                        if (buffer.Count == 0) continue;
//                        FillEmpty(ref filtered, buffer, pixel);
//                        buffer.Clear();
                    }
//                    else
//                    {
//                        buffer.Add(new Pixel(x,y));
//                    }
                }
            }
            var doubleFiltered = new Bitmap(filtered, 2, 2);
//            Console.WriteLine("NEW WIDTH: "+ doubleFiltered.Width);
            Console.WriteLine("totalPixels: " + totalPixels);
            Console.WriteLine("totalNewPixels: " + totalNewPixels);
            return filtered;
            
        }

        public static Color GetMyColor(Bitmap bitmap)
        {
            var square = FilteredBitmap(bitmap);
            var avgR = (square.GetPixel(0, 0).R + square.GetPixel(0, 1).R + square.GetPixel(1, 0).R + square.GetPixel(1, 1).R) /4;
            var avgG = (square.GetPixel(0, 0).G + square.GetPixel(0, 1).G + square.GetPixel(1, 0).G + square.GetPixel(1, 1).G) /4;
            var avgB = (square.GetPixel(0, 0).B + square.GetPixel(0, 1).B + square.GetPixel(1, 0).B + square.GetPixel(1, 1).B) /4;
            return Color.FromArgb(avgR, avgG, avgB);
        }

        private static void FillEmpty(ref Bitmap source, List<Pixel> tofill, Color color)
        {
            foreach (var pixel in tofill)
            {
                source.SetPixel(pixel.X, pixel.Y, color);
            }
        }

        private struct Pixel
        {
            public int X;
            public int Y;

            public Pixel(int x, int y)
            {
                this.Y = y;
                this.X = x;
            }
        }

        private static bool Filter(Color color)
        {
            if (color.R > 220 && color.G > 220 && color.B > 220)
            {
                return false;
            }

            if (color.R <= 10 && color.G <= 10 && color.B <= 10)
            {
                return false;
            }

            if (color.A < 200)
            {
                return false;
            }
            return true;
        }

        public static IEnumerable<Color> GetPixels(Bitmap bitmap)
        {
            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Color pixel = bitmap.GetPixel(x, y);
                    yield return pixel;
                }
            }
        }

        public static Color GetMostUsedColor(Bitmap theBitMap)
        {
            TenMostUsedColors = new List<Color>();
            TenMostUsedColorIncidences = new List<int>();

            MostUsedColor = Color.Empty;
            MostUsedColorIncidence = 0;

            // does using Dictionary<int,int> here
            // really pay-off compared to using
            // Dictionary<Color, int> ?

            // would using a SortedDictionary be much slower, or ?

            dctColorIncidence = new Dictionary<int, int>();

            // this is what you want to speed up with unmanaged code
            for (int row = 0; row < theBitMap.Size.Width; row++)
            {
                for (int col = 0; col < theBitMap.Size.Height; col++)
                {
                    pixelColor = theBitMap.GetPixel(row, col).ToArgb();

                    if (dctColorIncidence.Keys.Contains(pixelColor))
                    {
                        dctColorIncidence[pixelColor]++;
                    }
                    else
                    {
                        dctColorIncidence.Add(pixelColor, 1);
                    }
                }
            }

            // note that there are those who argue that a
            // .NET Generic Dictionary is never guaranteed
            // to be sorted by methods like this
            var dctSortedByValueHighToLow = dctColorIncidence.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

            // this should be replaced with some elegant Linq ?
            foreach (KeyValuePair<int, int> kvp in dctSortedByValueHighToLow.Take(10))
            {
                TenMostUsedColors.Add(Color.FromArgb(kvp.Key));
                TenMostUsedColorIncidences.Add(kvp.Value);
            }

            MostUsedColor = Color.FromArgb(dctSortedByValueHighToLow.First().Key);
            MostUsedColorIncidence = dctSortedByValueHighToLow.First().Value;

            return TenMostUsedColors[2];
        }

        public static Color GetAvgColor(Bitmap bitmap)
        {

            BitmapData srcData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly,
                PixelFormat.Format32bppRgb);

            int stride = srcData.Stride;

            IntPtr Scan0 = srcData.Scan0;

            long[] totals = new long[] { 0, 0, 0 };

            int width = bitmap.Width;
            int height = bitmap.Height;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        for (int color1 = 0; color1 < 3; color1++)
                        {
                            int idx = (y * stride) + x * 4 + color1;

                            totals[color1] += p[idx];
                        }
                    }
                }
            }

            int avgB = Convert.ToByte((totals[0] / (width * height))*1.25f);
            int avgG = Convert.ToByte((totals[1] / (width * height))*1.25f);
            int avgR = Convert.ToByte((totals[2] / (width * height)) * 1.25f);
            var colors = Color.FromArgb(avgR, avgG, avgB);
            return colors;
        }

    }
}