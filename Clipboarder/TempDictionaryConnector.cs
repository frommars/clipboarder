﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clipboarder
{
    public class TempDictionaryConnector: IDbConnector
    {
        private Dictionary<int, ClipData> _db;
        private bool _connected;
        private int _key = 0;
        public void ConnectToDb()
        {
            _db = new Dictionary<int, ClipData>();
            _connected = true;
        }

        public void AddRecord(object data)
        {
            if (!_connected)
            {
                throw new NullReferenceException();
            }

            var thisData = (ClipData) data;
            _db.Add(_key, thisData);
            thisData.Id = _key;          
            _key++;
        }

        public object GetRecord(object filter)
        {
            return _db[(int) filter];
        }

        public object GetAllRecords()
        {
            return _db.Values.ToList();
        }
    }

    public class ClipData
    {
        public object Data { get; set; }
        public decimal Size { get; set; }
        public int Id { get; set; }
    }
}
