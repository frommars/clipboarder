﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Clipboarder
{
    public static class TimerBuilder
    {
        public static Timer GetTimer()
        {
            return new Timer
            {
                Interval = 40
            };
        }
    }
}
