﻿using System.Windows;

namespace Clipboarder.UIBuilders
{
    public static class ContentTypeSolver
    {
        public static ContentType GetCurrentClipboardContentType()
        {
            if (Clipboard.ContainsText())
            {
                return ContentType.Text;
            }

            if (Clipboard.ContainsImage())
            {
                return ContentType.Image;
            }

            return ContentType.File;
        }
    }

    public enum ContentType
    {
        Text,
        Image,
        File
    }
}
