﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Image = System.Windows.Controls.Image;

namespace Clipboarder.UIBuilders
{
    public static class DataRectBuilder
    {
        public static Grid GetDataGrid()
        {
            var clipBoardOwnerProcess = Process.GetProcessById((int)NativeWinApi.GetClipboardOwnerProcessID());
            var pathToIcon = clipBoardOwnerProcess.MainModule.FileName;
            switch (ContentTypeSolver.GetCurrentClipboardContentType())
            {
                case ContentType.Text: return GetTextGrid(Clipboard.GetText(), pathToIcon);
                case ContentType.Image: return GetImageGrid(Clipboard.GetImage(), pathToIcon);
                    default: return GetTextGrid(Clipboard.GetText(), pathToIcon);
            }
        }

        public static Grid GetTextGrid(string text, string filePath)
        {
            var grid = new Grid();
            grid.MaxWidth = SystemParameters.PrimaryScreenWidth * 0.09;
            grid.Width = grid.MaxWidth;
            grid.Margin = new Thickness(5);

            var rect = new System.Windows.Shapes.Rectangle();
            rect.RadiusX = rect.RadiusY = 15;
            rect.Fill = new SolidColorBrush(Colors.AntiqueWhite);
            var brush = new SolidColorBrush(Colors.Yellow);
            grid.Children.Add(rect);

            var textBlock = new TextBlock
            {
                Text = text,
                FontFamily = new FontFamily("Consolas"),
                FontSize = 12,
                TextAlignment = TextAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Width = rect.Width,
                Height = rect.Height - rect.RadiusX * 2,
                Background = brush
            };
            //Add app icon
            var icon = new Image
            {
                //               Source = Shell.ImageSourceFilteredFromPath("C:\\Users\\Ilya\\Documents\\superIco.png"),
                Source = Shell.ImageSourceFilteredFromPath(filePath),
                Width = 48,
                Height = 48,
                HorizontalAlignment = HorizontalAlignment.Right,
                VerticalAlignment = VerticalAlignment.Top
            };

            var margin = new Thickness(0, icon.Height / 5, icon.Width / 5, 0);
            icon.Margin = margin;
            grid.Children.Add(textBlock);
            grid.Children.Add(icon);
            return grid;
        }

        public static Grid GetImageGrid(BitmapSource image, string filePath)
        {
            var grid = new Grid();
            grid.MaxWidth = SystemParameters.PrimaryScreenWidth * 0.09;
            grid.Width = grid.MaxWidth;
            grid.Margin = new Thickness(5);

            var rect = new System.Windows.Shapes.Rectangle();
            rect.RadiusX = rect.RadiusY = 15;
            rect.Fill = new SolidColorBrush(Colors.AntiqueWhite);
            var brush = new SolidColorBrush(Colors.Yellow);
            grid.Children.Add(rect);
            var imgBlock = new Image
            {
                Source = image
            };
            //            var textBlock = new TextBlock
            //            {
            //                Text = text,
            //                FontFamily = new FontFamily("Consolas"),
            //                FontSize = 12,
            //                TextAlignment = TextAlignment.Center,
            //                VerticalAlignment = VerticalAlignment.Center,
            //                Width = rect.Width,
            //                Height = rect.Height - rect.RadiusX * 2,
            //                Background = brush
            //            };
            //Add app icon
            var icon = new Image
            {
//               Source = Shell.ImageSourceFilteredFromPath("C:\\Users\\Ilya\\Documents\\superIco.png"),
                Source = Shell.ImageSourceFilteredFromPath(filePath),
                Width = 48,
                Height = 48,
                HorizontalAlignment = HorizontalAlignment.Right,
                VerticalAlignment = VerticalAlignment.Top
            };

            var margin = new Thickness(0, icon.Height / 5, icon.Width / 5, 0);
            icon.Margin = margin;
            grid.Children.Add(imgBlock);
            grid.Children.Add(icon);
            return grid;
        }
    }
}
